const express = require("express");
const cors = require('cors');
const {nanoid} = require('nanoid');
const app = express();
require('express-ws')(app);

const port = 8000;

app.use(cors());

const activeConnections = {};
const drawing = [];

app.ws('/draw', (ws, req) => {
  const id = nanoid();
  console.log('Client connected with id = ', id);
  activeConnections[id] = ws;
  ws.send(JSON.stringify({type: 'CONNECTED', message: drawing}));

  ws.on('message', msg => {
    const decoded = JSON.parse(msg);
    if (decoded.type === 'CREATE_DRAWING') {
      drawing.push(decoded.message);
      Object.keys(activeConnections).forEach(key => {
        const connection = activeConnections[key];
        connection.send(JSON.stringify({
          type: "NEW_DRAWING",
          message: decoded.message
        }));
      });
    }
  });

  ws.on('close', () => {
    console.log('Client disconnected with id = ', id);
    delete activeConnections[id];
  });
});

app.listen(port, () => {
  console.log(`Server started on ${port} port!`);

});
