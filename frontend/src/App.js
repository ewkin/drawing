import {useEffect, useRef, useState} from "react";

const App = () => {
  const [drawing, setDrawing] = useState({
    mouseDown: false,
    pixelsArray: []
  });

  const canvas = useRef(null);
  const ws = useRef(null);


  useEffect(() => {
    ws.current = new WebSocket('ws://localhost:8000/draw');
    ws.current.onmessage = e => {
      const decoded = JSON.parse(e.data);
      if (decoded.type === 'CONNECTED') {
        decoded.message.map((dot) => draw(dot.x, dot.y));
      }
      if (decoded.type === 'NEW_DRAWING') {
        draw(decoded.message.x, decoded.message.y);
      }
    };
  }, []);

  const canvasMouseMoveHandler = event => {
    if (drawing.mouseDown) {
      event.persist();
      const clientX = event.clientX;
      const clientY = event.clientY;
      setDrawing(prevState => {
        return {
          ...prevState,
          pixelsArray: [...prevState.pixelsArray, {
            x: clientX,
            y: clientY
          }]
        };
      });
      ws.current.send(JSON.stringify({
        type: 'CREATE_DRAWING',
        message: {
          x: clientX,
          y: clientY
        },
      }));
      draw(event.clientX, event.clientY);
    }
  };

  const draw = (x, y) => {
    const context = canvas.current.getContext('2d');
    const imageData = context.createImageData(1, 1);
    const d = imageData.data;

    d[0] = 0;
    d[1] = 0;
    d[2] = 0;
    d[3] = 255;

    context.putImageData(imageData, x, y);
  }

  const mouseDownHandler = event => {
    setDrawing({...drawing, mouseDown: true});
  };

  const mouseUpHandler = () => {
    setDrawing({...drawing, mouseDown: false, pixelsArray: []});
  };

  return (
    <div>
      <canvas
        ref={canvas}
        style={{border: '1px solid black'}}
        width={800}
        height={600}
        onMouseDown={mouseDownHandler}
        onMouseUp={mouseUpHandler}
        onMouseMove={canvasMouseMoveHandler}
      />
    </div>
  );
};
export default App;
